const names = {
	"Brandon": {
		"name": "Brandon Royd",
		"username": "brandon.royd",
		"age": 35
	},
	"Steve": {
		"name": "Style Tyler",
		"username": "style.tyler",
		"age": 56
	}
}

module.exports = {
	names: names
}

